package dcp.mc.projectsavethepets.fabric;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.fabric.api.client.keybinding.v1.KeyBindingHelper;
import net.minecraft.client.option.KeyBinding;
import org.lwjgl.glfw.GLFW;

public final class ClientInitImpl implements ClientModInitializer {
    private static KeyBinding allowDamageKeyBinding = null;

    public static KeyBinding getAllowDamageKeyBinding() {
        return allowDamageKeyBinding;
    }

    @Override
    public void onInitializeClient() {
        allowDamageKeyBinding = KeyBindingHelper.registerKeyBinding(new KeyBinding(
                "key.projectsavethepets.allowdamage",
                GLFW.GLFW_KEY_LEFT_SHIFT,
                "key.category.projectsavethepets"));
    }
}
