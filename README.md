# Project: Save the Pets!

**Version:** 4.0.0-alpha  
**License:** Apache-2.0  
**Client/Server Side:** Either (Limited Client features)  
**Forge/Fabric/Quilt:** Any

Protect your pets from yourself, friends, and the environment while giving them a second chance at life!

Works on the Client (with limited features) or the Server (Singleplayer counts as both). Unlike most mods, this mod will
work on both without the other. If installed on the server, all clients will receive server side features. If installed
on the client, you can join vanilla servers without issue.

Forge, Fabric, and Quilt have the same features and configuration files and can work with each other
(IE: Vanilla/Forge Server and Fabric/Vanilla Client).

## Dependencies

### Minecraft 1.19

**Java Version:** 17+

**Fabric Loader:** 0.14.0+  
**Fabric API:** 0.55.0+

**Forge Loader:** 41.0.0+

**Quilt Loader:** 0.17.0+  
**Quilted Fabric API:** 2.0.0-beta+

### Minecraft 1.18.0 - 1.18.2

**Java Version:** 17+

**Fabric Loader:** 0.14.0+  
**Fabric API:** 0.55.0+

**Forge Loader:** 41.0.0+

### Minecraft 1.16.5 - 1.17.1

**Java Version:** 17+

**Fabric Loader:** 0.14.0+  
**Fabric API:** 0.55.0+

**Forge Loader:** 41.0.0+

## Features

### Protection (Client/Server Side)

Your pets are protected from various sources of damage, either caused by you, your friends, or the environment.

#### Damage caused by Friends or You (Client/Server Side)

Damage that you or your friends cause can be prevented. The following sources are available and enabled by default:

* Direct (Client or Server)
* Explosions (Server only)
* Projectiles (Server only)
* Sweeping (Server only)

#### Environmental (Server Side Only)

Disabled by default for balancing reasons. The following can be enabled to protect your pets from them:

* Explosions
* Sweet Berry Bushes
* Fire/Lava
* Freezing from Powder Snow
* Drowning

### Whitelist (Client/Server Side)

You can whitelist friends that should be protected by the mod. You only need to provide the username or the UUID of the
account as the other values will be automatically generated. Behaviour is different between the server and client.

#### Server Side

Users added to the whitelist are protected from all players.

#### Client Side

Entities whose owners are in the whitelist are protected from damage caused by you (Limited by Client Side features).

### Vanilla Teams (Client/Server Side)

Players in the same vanilla team with Friendly Fire disabled cannot harm each other's pets. Disabled by default for
balancing reasons and Client Side bug.

**Note:** Client Side implementation might cause the game to freeze for a few seconds as Vanilla Teams uses usernames
rather than UUIDs.

### Revival (Server Side Only)

When your pet dies, it drops a note. You can then use this note to revive the pet by holding the note and right-clicking
on a Copper Block or other blocks that you have configured. The block will be consumed and the pet will be respawned! No
more missing your friend!

### Transferring Pets (Server Side Only)

You can assume ownership of a pet by holding a stick or other configurable items, crouching, and then right-clicking on
the pet.

However, this feature is disabled by default and users in the server's whitelist are protected from this. You can only
transfer pets that you are considered friends with (Same Vanilla Team or PvP is disabled).

### Untaming (Server Side Only)

Don't want your pet or want to give it to someone else? You can remove ownership by crouching and using a shear or other
configured items.

### Harming your pets (Client Side only)

Holding down the Shift key or the key you configured while attacking will bypass the Client Side protection and harm the
pet. I'm not sure why you want to do this, but it's here, just in case.

**Note:** Feature is removed if Server has the mod installed.

### Player Protection (Client/Server Side)

Disabled by default, you can add players to your whitelist (see whitelist for behaviour) and be unable to harm them or their pets!

**Note:** This feature could get you banned for changing how PvP works in servers. Use with caution.

## Configuration

The mod is highly configurable with options to disable or enable features to suit the experience the user wants to have.

The configuration file is based on JSON. Please see the file [CONFIG.md]() for an example with comments.

## APIs (For Devs)

This mod is designed to be highly configurable from the programming aspects to allow other mods to adjust or add
behaviour to the mod.

Many methods and interfaces are available to use to easily add new environmental protection, new entity type to be
protected, or even logic to handle how protection is done (for Factions and similarly based mods).

I suggest having a look at the source code for ideas. Main areas to check are:

* `dcp.mc.projectsavethepets.ProjectSaveThePets` - Main instance and logic handler
* `dcp.mc.projectsavethepets.apis.*` - Contains interfaces that is used by the mod
* `dcp.mc.projectsavethepets.blockers.*` - Code to prevent friendly fire
* `dcp.mc.projectsavethepets.entities.*` - Entities that are protected
* `dcp.mc.projectsavethepets.mixins.*` - Used to protect from sources of damage
* `dcp.mc.projectsavethepets.notes.*` - Revival Notes NBT handlers

## FAQ

### Forge?

It's done already! I also updated it to include the Quilt Loader as well!

### Mod Packs?

Go for it! You have my full permission to add the mod to your Public or Private Mod Pack. No credits required!

### Can I use this for my mod?

Sure thing! Same as above!

### Hey! I saw your code and would like to take a snippet!

Sure, I don't mind! As long as it's not a blatant copy and paste of an entire file! I do wish to be mentioned if
possible but isn't a requirement.

### Do I need Fabric API/Quilted Fabric API?

Yes you do as the mod needs it for the custom keybindings. But only if you are using the Fabric/Quilt version of the
mod!

### Is this client or server sided?

It can run on the Client without the Server having it installed. But, many features will be missing. This is more useful
for playing on a Public Vanilla Servers with your friends, so that you don't start a war between your pets. **Note:** We
are not responsible for bans for using this mod in a public server.

However, if the server has it installed, no Players will need to install the mod to get access to all the features.

Enjoy!

### Does my client need this mod?

Only for servers that don't have this mod and for Singleplayer/LAN! See above for more information.

### Does LAN Multiplayer work with this mod?

It does! As long as the person who is hosting the session has the mod, then everyone will have access to all features!

### I got a question or a bug.

Feel free to comment below the question, however I would prefer if you left any bug reports in the GitLab Repository!
Thanks!

### I got a suggestion or feature I want added!

As above, feel free to comment it! I do my best to add it, even if I disagree with it for balance reasons (Such as
environmental protection). I feel as if the mod should fit your needs, even if it is a niche. So it might be disabled in
the config by default, but you can always enable it!
