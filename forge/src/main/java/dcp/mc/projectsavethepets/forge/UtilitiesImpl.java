package dcp.mc.projectsavethepets.forge;

import java.nio.file.Path;
import net.minecraft.client.option.KeyBinding;
import net.minecraftforge.fml.loading.FMLPaths;

public class UtilitiesImpl {
    public static KeyBinding getAllowDamageKeybinding() {
        return ClientInitImpl.getAllowDamageKeyBinding();
    }

    public static Path getConfigDirectory() {
        return FMLPaths.CONFIGDIR.get();
    }
}
