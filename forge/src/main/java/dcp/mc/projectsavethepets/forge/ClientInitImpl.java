package dcp.mc.projectsavethepets.forge;

import net.minecraft.client.option.KeyBinding;
import net.minecraftforge.client.event.RegisterKeyMappingsEvent;
import org.lwjgl.glfw.GLFW;

public class ClientInitImpl {
    private static KeyBinding allowDamageKeyBinding = null;

    public static KeyBinding getAllowDamageKeyBinding() {
        return allowDamageKeyBinding;
    }

    public static void onRegisterKeyMappingsEvent(RegisterKeyMappingsEvent event) {
        allowDamageKeyBinding = new KeyBinding(
                "key.projectsavethepets.allowdamage",
                GLFW.GLFW_KEY_LEFT_SHIFT,
                "key.category.projectsavethepets");
        event.register(allowDamageKeyBinding);
    }
}
