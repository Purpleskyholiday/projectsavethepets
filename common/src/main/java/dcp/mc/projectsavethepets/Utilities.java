package dcp.mc.projectsavethepets;

import dcp.mc.projectsavethepets.mixins.accessors.EntityAccessor;
import dev.architectury.injectables.annotations.ExpectPlatform;
import java.nio.file.Path;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import net.minecraft.client.option.KeyBinding;
import net.minecraft.entity.Entity;
import net.minecraft.entity.data.TrackedData;
import net.minecraft.util.Identifier;
import org.jetbrains.annotations.ApiStatus;

@ApiStatus.Internal
public final class Utilities {
    public static final Utilities INSTANCE = new Utilities();
    private final Map<String, Identifier> identifierCache = new ConcurrentHashMap<>();

    private Utilities() {
    }

    public <E> E getTrackableData(Entity entity, TrackedData<E> field) {
        return ((EntityAccessor) entity).getDataTracker().get(field);
    }

    public <E> void setTrackableData(Entity entity, TrackedData<E> field, E value) {
        ((EntityAccessor) entity).getDataTracker().set(field, value);
    }

    @ExpectPlatform
    public static KeyBinding getAllowDamageKeybinding() {
        throw new AssertionError();
    }

    @ExpectPlatform
    public static Path getConfigDirectory() {
        throw new AssertionError();
    }

    public Identifier convertToId(String id) {
        return identifierCache.computeIfAbsent(id, key -> Identifier.splitOn(key, ':'));
    }
}
