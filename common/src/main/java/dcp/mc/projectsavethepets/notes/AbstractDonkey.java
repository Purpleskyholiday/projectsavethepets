package dcp.mc.projectsavethepets.notes;

import dcp.mc.projectsavethepets.apis.NoteGeneratorApi;
import net.minecraft.entity.passive.AbstractDonkeyEntity;
import net.minecraft.nbt.NbtCompound;
import org.jetbrains.annotations.NotNull;

public final class AbstractDonkey implements NoteGeneratorApi<AbstractDonkeyEntity> {
    public static final AbstractDonkey INSTANCE = new AbstractDonkey();

    private AbstractDonkey() {
    }

    @Override
    public @NotNull Class<AbstractDonkeyEntity> type() {
        return AbstractDonkeyEntity.class;
    }

    @Override
    public void setupNbt(@NotNull AbstractDonkeyEntity entity, @NotNull NbtCompound nbt) {
        nbt.remove("ChestedHorse");
        nbt.remove("Items");
    }
}
