package dcp.mc.projectsavethepets.notes;

import dcp.mc.projectsavethepets.apis.NoteGeneratorApi;
import net.minecraft.entity.passive.HorseEntity;
import net.minecraft.nbt.NbtCompound;
import org.jetbrains.annotations.NotNull;

public final class Horse implements NoteGeneratorApi<HorseEntity> {
    public static final Horse INSTANCE = new Horse();

    private Horse() {
    }

    @Override
    public @NotNull Class<HorseEntity> type() {
        return HorseEntity.class;
    }

    @Override
    public void setupNbt(@NotNull HorseEntity entity, @NotNull NbtCompound nbt) {
        nbt.remove("ArmorItem");
    }
}
