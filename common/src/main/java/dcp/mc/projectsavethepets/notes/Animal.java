package dcp.mc.projectsavethepets.notes;

import dcp.mc.projectsavethepets.apis.NoteGeneratorApi;
import net.minecraft.entity.passive.AnimalEntity;
import net.minecraft.nbt.NbtCompound;
import org.jetbrains.annotations.NotNull;

public final class Animal implements NoteGeneratorApi<AnimalEntity> {
    public static final Animal INSTANCE = new Animal();

    private Animal() {
    }

    @Override
    public @NotNull Class<AnimalEntity> type() {
        return AnimalEntity.class;
    }

    @Override
    public void setupNbt(@NotNull AnimalEntity entity, @NotNull NbtCompound nbt) {
        nbt.remove("InLove");
        nbt.remove("LoveCause");
    }
}
