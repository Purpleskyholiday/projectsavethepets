package dcp.mc.projectsavethepets.blockers;

import dcp.mc.projectsavethepets.apis.FriendlyFireBlockerApi;
import java.util.UUID;
import net.minecraft.entity.player.PlayerEntity;
import org.jetbrains.annotations.NotNull;

public final class OwnerBlocker implements FriendlyFireBlockerApi {
    public static final OwnerBlocker INSTANCE = new OwnerBlocker();

    private OwnerBlocker() {
    }

    @Override
    public boolean preventDamage(@NotNull PlayerEntity attacker, @NotNull UUID owner) {
        return attacker.getUuid().equals(owner);
    }
}
