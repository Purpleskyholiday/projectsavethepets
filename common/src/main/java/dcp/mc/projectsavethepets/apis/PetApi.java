package dcp.mc.projectsavethepets.apis;

import java.util.UUID;
import net.minecraft.entity.Entity;
import org.jetbrains.annotations.NotNull;

public interface PetApi<T extends Entity> {
    @NotNull Class<T> type();

    boolean isPet(@NotNull T entity);

    @NotNull UUID[] getOwners(@NotNull T entity);
}
