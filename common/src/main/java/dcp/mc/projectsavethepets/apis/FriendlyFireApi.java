package dcp.mc.projectsavethepets.apis;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import org.jetbrains.annotations.NotNull;

public interface FriendlyFireApi<T extends Entity> {
    @NotNull Class<T> type();

    boolean allowDamage(@NotNull PlayerEntity attacker, @NotNull T target);
}
