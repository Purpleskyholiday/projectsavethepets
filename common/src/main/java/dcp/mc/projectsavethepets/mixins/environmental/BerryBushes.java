package dcp.mc.projectsavethepets.mixins.environmental;

import dcp.mc.projectsavethepets.ProjectSaveThePets;
import dcp.mc.projectsavethepets.config.Config;
import net.minecraft.block.BlockState;
import net.minecraft.block.SweetBerryBushBlock;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(value = SweetBerryBushBlock.class)
final class BerryBushes {
    @Inject(method = "onEntityCollision", at = @At(value = "HEAD"), cancellable = true)
    void preventDamage(BlockState state, World world, BlockPos pos, Entity entity, CallbackInfo ci) {
        if (Config.INSTANCE.getEnvironmentalProtection().isBushes() && ProjectSaveThePets.INSTANCE.isPet(entity)) {
            ci.cancel();
        }
    }
}
