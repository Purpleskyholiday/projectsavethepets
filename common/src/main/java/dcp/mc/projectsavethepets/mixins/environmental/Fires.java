package dcp.mc.projectsavethepets.mixins.environmental;

import dcp.mc.projectsavethepets.ProjectSaveThePets;
import dcp.mc.projectsavethepets.config.Config;
import net.minecraft.entity.Entity;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(Entity.class)
final class Fires {
    @Inject(method = "isFireImmune", at = @At(value = "HEAD"), cancellable = true)
    private void preventFireDamage(CallbackInfoReturnable<Boolean> cir) {
        if (Config.INSTANCE.getEnvironmentalProtection().isFires() && ProjectSaveThePets.INSTANCE.isPet((Entity) (Object) this)) {
            cir.setReturnValue(true);
        }
    }
}
