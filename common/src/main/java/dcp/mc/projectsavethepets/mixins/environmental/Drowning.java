package dcp.mc.projectsavethepets.mixins.environmental;

import dcp.mc.projectsavethepets.ProjectSaveThePets;
import dcp.mc.projectsavethepets.config.Config;
import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(value = LivingEntity.class)
final class Drowning {
    @Inject(method = "canBreatheInWater", at = @At(value = "HEAD"), cancellable = true)
    private void preventDrowning(CallbackInfoReturnable<Boolean> cir) {
        if (Config.INSTANCE.getEnvironmentalProtection().isDrowning() && ProjectSaveThePets.INSTANCE.isPet((Entity) (Object) this)) {
            cir.setReturnValue(true);
        }
    }
}
