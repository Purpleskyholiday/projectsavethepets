package dcp.mc.projectsavethepets.mixins.environmental;

import dcp.mc.projectsavethepets.ProjectSaveThePets;
import dcp.mc.projectsavethepets.config.Config;
import net.minecraft.entity.Entity;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(Entity.class)
final class Explosions {
    @Inject(method = "isImmuneToExplosion", at = @At(value = "HEAD"), cancellable = true)
    private void preventExplosionDamage(CallbackInfoReturnable<Boolean> cir) {
        if (Config.INSTANCE.getEnvironmentalProtection().isExplosions() && ProjectSaveThePets.INSTANCE.isPet((Entity) (Object) this)) {
            cir.setReturnValue(true);
        }
    }
}
