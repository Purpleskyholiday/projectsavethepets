package dcp.mc.projectsavethepets;

import dcp.mc.projectsavethepets.apis.FriendlyFireApi;
import dcp.mc.projectsavethepets.apis.FriendlyFireBlockerApi;
import dcp.mc.projectsavethepets.apis.NoteGeneratorApi;
import dcp.mc.projectsavethepets.apis.OwnershipApi;
import dcp.mc.projectsavethepets.apis.PetApi;
import dcp.mc.projectsavethepets.apis.TransferBlockerApi;
import dcp.mc.projectsavethepets.blockers.OwnerBlocker;
import dcp.mc.projectsavethepets.blockers.PvPBlocker;
import dcp.mc.projectsavethepets.blockers.VanillaTeamBlocker;
import dcp.mc.projectsavethepets.blockers.WhitelistBlocker;
import dcp.mc.projectsavethepets.config.Config;
import dcp.mc.projectsavethepets.entities.AbstractHorse;
import dcp.mc.projectsavethepets.entities.Fox;
import dcp.mc.projectsavethepets.entities.Player;
import dcp.mc.projectsavethepets.entities.Tamable;
import dcp.mc.projectsavethepets.notes.AbstractDonkey;
import dcp.mc.projectsavethepets.notes.Animal;
import dcp.mc.projectsavethepets.notes.Base;
import dcp.mc.projectsavethepets.notes.Horse;
import dcp.mc.projectsavethepets.notes.Living;
import dcp.mc.projectsavethepets.notes.Mob;
import dcp.mc.projectsavethepets.notes.Wolf;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArraySet;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.NbtCompound;

public final class ProjectSaveThePets {
    public static final ProjectSaveThePets INSTANCE = new ProjectSaveThePets();
    public final Set<FriendlyFireBlockerApi> FRIENDLY_FIRE_BLOCKER_APIS = new CopyOnWriteArraySet<>();
    public final Set<TransferBlockerApi> TRANSFER_BLOCKER_APIS = new CopyOnWriteArraySet<>();
    public final Set<NoteGeneratorApi<?>> NOTE_GENERATOR_APIS = new CopyOnWriteArraySet<>();
    public final Set<FriendlyFireApi<?>> FRIENDLY_FIRE_APIS = new CopyOnWriteArraySet<>();
    public final Set<OwnershipApi<?>> OWNERSHIP_APIS = new CopyOnWriteArraySet<>();
    public final Set<PetApi<?>> PET_APIS = new CopyOnWriteArraySet<>();

    private ProjectSaveThePets() {
        if (Config.INSTANCE.getProtectedEntities().isTamable()) {
            NOTE_GENERATOR_APIS.add(Tamable.INSTANCE);
            NOTE_GENERATOR_APIS.add(Wolf.INSTANCE);
            OWNERSHIP_APIS.add(Tamable.INSTANCE);
            PET_APIS.add(Tamable.INSTANCE);
        }

        if (Config.INSTANCE.getProtectedEntities().isFoxes()) {
            NOTE_GENERATOR_APIS.add(Fox.INSTANCE);
            OWNERSHIP_APIS.add(Fox.INSTANCE);
            PET_APIS.add(Fox.INSTANCE);
        }

        if (Config.INSTANCE.getProtectedEntities().isHorses()) {
            NOTE_GENERATOR_APIS.add(AbstractDonkey.INSTANCE);
            NOTE_GENERATOR_APIS.add(AbstractHorse.INSTANCE);
            NOTE_GENERATOR_APIS.add(Horse.INSTANCE);
            OWNERSHIP_APIS.add(AbstractHorse.INSTANCE);
            PET_APIS.add(AbstractHorse.INSTANCE);
        }

        if (Config.INSTANCE.getProtectedEntities().isPlayers()) {
            FRIENDLY_FIRE_APIS.add(Player.INSTANCE);
            PET_APIS.add(Player.INSTANCE);
        }

        if (Config.INSTANCE.getDamageProtection().isVanillaTeams()) {
            FRIENDLY_FIRE_BLOCKER_APIS.add(VanillaTeamBlocker.INSTANCE);
        }

        if (Config.INSTANCE.getPlayerWhitelist().length > 0) {
            FRIENDLY_FIRE_BLOCKER_APIS.add(WhitelistBlocker.INSTANCE);
            TRANSFER_BLOCKER_APIS.add(WhitelistBlocker.INSTANCE);
        }

        FRIENDLY_FIRE_BLOCKER_APIS.add(OwnerBlocker.INSTANCE);
        FRIENDLY_FIRE_BLOCKER_APIS.add(PvPBlocker.INSTANCE);

        NOTE_GENERATOR_APIS.add(Animal.INSTANCE);
        NOTE_GENERATOR_APIS.add(Living.INSTANCE);
        NOTE_GENERATOR_APIS.add(Base.INSTANCE);
        NOTE_GENERATOR_APIS.add(Mob.INSTANCE);
    }

    private <T extends Entity> boolean allowDamage(FriendlyFireApi<T> api, PlayerEntity player, Entity entity) {
        return api.type().isInstance(entity) && api.allowDamage(player, api.type().cast(entity));
    }

    private <T extends Entity> UUID[] getOwners(PetApi<T> api, Entity entity) {
        return api.type().isInstance(entity) ? api.getOwners(api.type().cast(entity)) : new UUID[0];
    }

    private <T extends Entity> boolean isPet(PetApi<T> api, Entity entity) {
        return api.type().isInstance(entity) && api.isPet(api.type().cast(entity));
    }

    private <T extends Entity> boolean isOwner(OwnershipApi<T> api, Entity entity, UUID owner) {
        return api.type().isInstance(entity) && api.isOwner(api.type().cast(entity), owner);
    }

    private <T extends Entity> void setupNbt(NoteGeneratorApi<T> api, Entity entity, NbtCompound data) {
        if (api.type().isInstance(entity)) {
            api.setupNbt(api.type().cast(entity), data);
        }
    }

    private <T extends Entity> boolean removeOwnership(OwnershipApi<T> api, PlayerEntity player, Entity entity) {
        return api.type().isInstance(entity) && api.removeOwnership(api.type().cast(entity), player.getUuid());
    }

    private <T extends Entity> boolean transferOwnership(OwnershipApi<T> api, PlayerEntity player, Entity entity) {
        return api.type().isInstance(entity) && api.transferOwnership(api.type().cast(entity), player.getUuid());
    }

    @SuppressWarnings("deprecation")
    private boolean isBlacklisted(Entity entity) {
        for (String entry : Config.INSTANCE.getProtectedEntities().getBlacklist()) {
            if (entity.getType().getRegistryEntry().matchesId(Utilities.INSTANCE.convertToId(entry))) {
                return true;
            }
        }

        return false;
    }

    public boolean isFriendly(PlayerEntity player, Entity entity) {
        if (isBlacklisted(entity)) {
            return false;
        }

        for (FriendlyFireApi<?> api : FRIENDLY_FIRE_APIS) {
            if (allowDamage(api, player, entity)) {
                return false;
            }
        }

        for (PetApi<?> petApi : PET_APIS) {
            UUID[] owners = getOwners(petApi, entity);

            for (UUID owner : owners) {
                for (FriendlyFireBlockerApi api : FRIENDLY_FIRE_BLOCKER_APIS) {
                    if (api.preventDamage(player, owner)) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    public boolean removeOwnership(PlayerEntity player, Entity entity) {
        if (isBlacklisted(entity)) {
            return false;
        }

        for (OwnershipApi<?> api : OWNERSHIP_APIS) {
            if (isOwner(api, entity, player.getUuid())) {
                if (removeOwnership(api, player, entity)) {
                    return true;
                }
            }
        }

        return false;
    }

    public boolean transferOwnership(PlayerEntity player, Entity entity) {
        if (!isFriendly(player, entity)) {
            return false;
        }

        for (PetApi<?> petApi : PET_APIS) {
            UUID[] owners = getOwners(petApi, entity);

            for (UUID owner : owners) {
                for (TransferBlockerApi api : TRANSFER_BLOCKER_APIS) {
                    if (api.preventTransfer(player, owner)) {
                        return false;
                    }
                }
            }
        }

        for (OwnershipApi<?> api : OWNERSHIP_APIS) {
            if (transferOwnership(api, player, entity)) {
                return true;
            }
        }

        return false;
    }

    public boolean isPet(Entity entity) {
        if (isBlacklisted(entity)) {
            return false;
        }

        for (PetApi<?> petApi : PET_APIS) {
            if (isPet(petApi, entity)) {
                return true;
            }
        }

        return false;
    }

    public void setupNbt(Entity entity, NbtCompound data) {
        for (NoteGeneratorApi<?> api : NOTE_GENERATOR_APIS) {
            setupNbt(api, entity, data);
        }
    }
}
